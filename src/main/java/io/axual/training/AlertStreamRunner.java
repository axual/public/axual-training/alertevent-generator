package io.axual.training;

import io.axual.general.Application;

import io.axual.general.User;
import io.axual.interaction.AlertChannelType;
import io.axual.interaction.AlertEvent;

import io.axual.payments.AccountAuthorization;
import io.axual.payments.AccountEntryEvent;
import io.axual.payments.IBAN;
import io.axual.training.common.AmountUtils;

import io.axual.training.properties.StreamsProperties;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static io.axual.training.common.AmountUtils.greaterThanOrEquals;
import static io.axual.training.common.AmountUtils.smallerThan;

import java.math.BigDecimal;
import java.util.*;

@Component
public final class AlertStreamRunner {
    private static final Logger LOG = LoggerFactory.getLogger(AlertStreamRunner.class);

    private final StreamsProperties streamsProperties;

    public AlertStreamRunner(StreamsProperties streamsProperties) {
        LOG.info("Creating Kafka Streams with properties {}", streamsProperties.asProperties());
        this.streamsProperties = streamsProperties;
    }

    public void start() {
        final StreamsBuilder builder = new StreamsBuilder();

        // Build a topology combining both the "payments-accountentry" and "payments-accountauthorization" topics, and generate an alert on the
        // "interaction-alert". 
        // Topic "interaction-alert" Key is the User object and Value is the AlertEvent object
        // Topic "payments-accountentry" Key is the AccountId object and Value is the AccountEntryEvent object
        // Topic "payments-accountauthorization" Key is the AccountId object and Value is the AccountAuthorization object
        // TIP 1: use the methods already present in this class to create the right events
        // TIP 2: Kafka Stream and table objects can be joined using a value joiner method to create a new Stream object, joins are determined by matching Keys
        // TIP 3: Kafka Stream objects can be mapped or flat mapped to new Stream objects
        // TIP 4: check https://docs.confluent.io/current/streams/quickstart.html



        final Topology topology = builder.build();
        final KafkaStreams streams = new KafkaStreams(topology,streamsProperties.asProperties());
        streams.start();
    }

    private List<AlertEvent> generateAlerts(AccountEntryEvent accountEntryEvent, AccountAuthorization accountAuthorization) {
        List<AlertEvent> alertEvents = new ArrayList<>();
        if (accountAuthorization == null) {
            return alertEvents;
        }
        String alertType = null;
        if (balanceDroppedBelowThreshold(accountEntryEvent, BigDecimal.ZERO)) {
            alertType = "balance-below-zero";
        } else if (balanceRoseAboveThreshold(accountEntryEvent, BigDecimal.ZERO)) {
            alertType = "balance-above-zero";
        }
        if (alertType == null) {
            return alertEvents;
        }
        for (User user : accountAuthorization.getUsers()) {
            generateAlert(alertEvents, alertType, accountEntryEvent, user);
        }
        return alertEvents;
    }

    private void generateAlert(List<AlertEvent> alertEvents, String alertType, AccountEntryEvent accountEntryEvent, User user) {
        {
            AlertEvent alertEvent =
                    AlertEvent.newBuilder()
                            .setSource(Application.newBuilder()
                                    .setName(this.getClass().getName())
                                    .setVersion("1.0.0-SNAPSHOT")
                                    .build())
                            .setAlertType(alertType)
                            .setTimestamp(accountEntryEvent.getTimestamp())
                            .setChannel(AlertChannelType.PUSH)
                            .setUser(user)
                            .setParams(getParams(accountEntryEvent))
                            .build();
            alertEvents.add(alertEvent);
        }
    }

    private static boolean balanceDroppedBelowThreshold(AccountEntryEvent accountEntryEvent, BigDecimal threshold) {
        // balanceBefore >= treshold
        // balanceAfter < treshold
        return greaterThanOrEquals(getBalanceBeforeTransaction(accountEntryEvent), threshold) && smallerThan(AmountUtils.getBigDecimal(accountEntryEvent.getBalanceAfterTransaction()), threshold);
    }

    private static boolean balanceRoseAboveThreshold(AccountEntryEvent accountEntryEvent, BigDecimal threshold) {
        // balanceBefore < treshold
        // balanceAfter >= treshold
        return smallerThan(getBalanceBeforeTransaction(accountEntryEvent), threshold) && greaterThanOrEquals(AmountUtils.getBigDecimal(accountEntryEvent.getBalanceAfterTransaction()), threshold);
    }

    private static Map<String, String> getParams(AccountEntryEvent accountEntryEvent) {
        Map<String, String> params = new HashMap<>();
        params.put("iban", ((IBAN) accountEntryEvent.getAccount().getId()).getAccountNumber());
        params.put("balanceAfter", AmountUtils.getFormattedAmount(accountEntryEvent.getBalanceAfterTransaction(), accountEntryEvent.getCurrency().getSymbol(), true));
        params.put("amount", AmountUtils.getFormattedAmount(accountEntryEvent.getAmount(), accountEntryEvent.getCurrency().getSymbol(), true));
        return params;
    }

    private static BigDecimal getBalanceBeforeTransaction(AccountEntryEvent accountEntryEvent) {
        BigDecimal balanceAfterTransaction = AmountUtils.getBigDecimal(accountEntryEvent.getBalanceAfterTransaction());
        BigDecimal amount = AmountUtils.getBigDecimal(accountEntryEvent.getAmount());
        switch (accountEntryEvent.getCreditDebitIndicator()) {
            case CREDIT:
                return balanceAfterTransaction.subtract(amount);
            case DEBIT:
                return balanceAfterTransaction.add(amount);
            default:
                throw new RuntimeException("Unknown CreditDebetType");
        }
    }
}
